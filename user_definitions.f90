subroutine user_definitions
use global
implicit none

     nele=250;       !number of elements
     nnode=nele+1;   !number of nodes
     medges=2;       !number of edges for each cell
     mvariables=7;   !number of variables
     lx =4.0d0      !length of domain
     minx=-2.0d0;     !the minimum x coordinates, the left_end of the domain
     CFL=0.5d0;      !CFL number
	 rhos=2680.0d0;
	 rhow=1000.0d0;
	 por=0.4d0;		  !porosity of the bed material
	 rho0=(1.0d0-por)*rhos+por*rhow;
	 !rr=0.98d0;       !density ratio
	 eps=1.0d-18
	 man1=0.01d0      !interface manning type friction coefficient  
	 man2=0.03d0      !bottom manning coefficient
	 dd=2.0d-4        !mean partical diameter
	 nnu=1.0d-6       ! kinematic viscosity of water
	 ! (Zhang and Xie,1993) settling velocity of sediment
	omegaV=dsqrt((13.95d0*nnu/dd)**2.0d0+1.09d0*(rhos/rhow-1.0d0)*g*dd)-13.95d0*nnu/dd 	
	Rp0=dd*omegaV/nnu   ! terminal Reynolds number
	Rp1=dd*dsqrt(g*(rhos/rhow-1.0d0)*dd)/nnu     ! particle Reynolds number 
	
     allocate(x(nnode),xc(nele),Bn(nnode),Bdx(nele),Bold(nele),&
         h2old(nele),uh2old(nele),u2old(nele),eta2old(nele),&
		 h1old(nele),uh1old(nele),u1old(nele),eta1old(nele),&
         nodnumber(nnode),neigh(nele,medges),celltype2(nele),&
         ele(nele,medges),U(nele,mvariables),axp(nnode),eta1aux(nele),&
         axm(nnode),F(nnode,mvariables),dtlimit(nele),dtdrain2(nele),&
         lmx(nele,mvariables),Uc(nele,mvariables),Un(nele,mvariables),&
         UL(nnode,mvariables),UR(nnode,mvariables),FL(nnode,mvariables),&
         FR(nnode,mvariables),eta2old0(nele),uh2old0(nele),&
		 h2R(nnode),h2L(nnode),dtedge2(nnode),celltype(nele),eta2aux(nele),&
		 eta2xleft(nele),eta2xright(nele),sourceterm2(nele),&
		 eta1xleft(nele),eta1xright(nele),h1old0(nele),uh1old0(nele),&
		 h1R(nnode),h1L(nnode),ump(nnode),umm(nnode),dtdrain1(nele),&
		 dtedge1(nnode),sourceterm1(nele),dtedge3(nnode),&
		 h1t0(nele),eta2t0(nele),Es(nele),Ds(nele),Ew(nele),&
		 Reta2old(nele),dtdrain3(nele),tau1(nele),tau2(nele),&
		 rho2(nele),psR1(nele),psR2(nele),psR3(nele),&
		 psR4(nele),psR5(nele),psR6(nele),psR7(nele),cold(nele),&
		 h3R(nnode),h3L(nnode),hcR(nnode),hcL(nnode),&
		 rho2R(nnode),rho2L(nnode),Cnc2(nele),Cnc4(nele),&
		 ncsource2(nele),ncsource4(nele),CC2(nele),CC4(nele),&
		 hcold(nele),h1x(nele),RichNo(nele),phi9(nele),Bxs(nele),&
		 hcold0(nele),Reta2old0(nele),Bold0(nele),ce(nele),qb0(nele),&
		 stress(nele),tau20(nele),Reynold(nele),thetac(nele),problem(nele),&
		 cnb(nele),Sfx(nele),XE(nele),usk(nele),fman1(nele),fman2(nele))   


end subroutine user_definitions
