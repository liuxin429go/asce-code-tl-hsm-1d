module global   !define arrays

integer, parameter :: DP = kind(1.0d0)   ! double precision parameter
real(DP) start,finish,eps,delx,dely,dt,th,lx,minx,time,totaltime,CFL,rr,Ustar
real(DP) errh1,erreta2,erruh1,erruh2,Linferrh1,Linferreta2,Linferruh1,Linferruh2
real(DP) rhos,rhow,por,man1,man2,dd,omegaV,nnu,rho0,Rp0,coeffb,assdeph2,kappa0,Rp1
integer nn,i,j,k,m,nele,nx9,ny9,i2,rkd,nnode,mvariables,medges,i3
integer,allocatable,save:: neigh(:,:),ele(:,:),nodnumber(:),celltype(:),celltype2(:)
real(DP),allocatable,save:: x(:),xc(:),Bn(:),Bdx(:),Bold(:),U(:,:),dtedge3(:)
real(DP),allocatable,save:: h2old(:),uh2old(:),u2old(:),eta2old(:),thetac(:) 
real(DP),allocatable,save:: h1old(:),uh1old(:),u1old(:),eta1old(:) 
real(DP),allocatable,save:: axp(:),axm(:),F(:,:),dtlimit(:),dtedge2(:)
real(DP),allocatable,save:: lmx(:,:),Uc(:,:),Un(:,:),UL(:,:),UR(:,:),FL(:,:),FR(:,:)
real(DP),allocatable,save:: eta2old0(:),uh2old0(:),dtdrain2(:),sourceterm2(:)
real(DP),allocatable,save:: h2R(:),h2L(:),eta1aux(:),eta2aux(:),dtedge1(:)
real(DP),allocatable,save:: eta2xleft(:),eta2xright(:),h1old0(:),uh1old0(:)
real(DP),allocatable,save:: hcold0(:),Reta2old0(:),Bold0(:),tau20(:),Reynold(:)
real(DP),allocatable,save:: h1R(:),h1L(:),ump(:),umm(:),sourceterm1(:),problem(:)
real(DP),allocatable,save:: eta1xleft(:),eta1xright(:),dtdrain1(:),Ew(:)
real(DP),allocatable,save:: h1t0(:),eta2t0(:),Es(:),Ds(:),rho2(:),cold(:)
real(DP),allocatable,save:: Reta2old(:),dtdrain3(:),tau1(:),tau2(:),stress(:)
real(DP),allocatable,save:: psR1(:),psR2(:),psR3(:),psR4(:),psR5(:),psR6(:),psR7(:)
real(DP),allocatable,save:: h3R(:),h3L(:),hcR(:),hcL(:),rho2R(:),rho2L(:),cnb(:)
real(DP),allocatable,save:: Cnc2(:),Cnc4(:),CC2(:),CC4(:),ncsource2(:),ncsource4(:)
real(DP),allocatable,save:: hcold(:),h1x(:),RichNo(:),phi9(:),Bxs(:),ce(:),qb0(:)
real(DP),allocatable,save:: Sfx(:),XE(:),usk(:),fman1(:),fman2(:)


real(DP), parameter :: pi=4.0d0*datan(1.0d0)	 ! Pi
real(DP), parameter :: g=9.81d0                ! g

Contains

    elemental real(DP) function Minmod(x,y,z)
    ! minmod for estimating slopes,
      real(DP), intent(in) :: x,y,z
      !if x,y, z are all positive, then output is the minimum
      if (dmin1(x,y,z)>0.0d0) then
        Minmod=dmin1(x,y,z)
      ! if x, y, and z are all negative, then the output is the maximum
      else if (dmax1(x,y,z)<0.0d0) then
        Minmod=dmax1(x,y,z)
      ! if x, y and z have mixed signs, output is 0
      else
        Minmod=0.0d0
      end if
    end function Minmod

end module global
