# Start of the makefile
# Defining variables


objects =   global.o \
            user_definitions.o\
		    generate_mesh_1D.o\
            initial_condition.o\
			boundary_conditions.o\
			Numeric_Flux_approx.o\
			Subroutines.o\
            SW_Solver_1D.o\
            main.o \

f90comp = gfortran
fflags = -g

%.o : %.mod

# Makefile
main: $(objects)
	$(f90comp) $(fflags) -o hmX_1D $^
main.o: main.f90 SW_Solver_1D.o Subroutines.o Numeric_Flux_approx.o boundary_conditions.o initial_condition.o generate_mesh_1D.o user_definitions.o global.mod
	$(f90comp) -c $(fflags) $<
SW_Solver_1D.o: SW_Solver_1D.f90 Subroutines.o Numeric_Flux_approx.o boundary_conditions.o initial_condition.o generate_mesh_1D.o user_definitions.o global.mod
	$(f90comp) -c $(fflags) $<
Subroutines.o: Subroutines.f90 Numeric_Flux_approx.o boundary_conditions.o initial_condition.o generate_mesh_1D.o user_definitions.o global.mod
	$(f90comp) -c $(fflags) $<
Numeric_Flux_approx.o: Numeric_Flux_approx.f90 boundary_conditions.o initial_condition.o global.mod user_definitions.o generate_mesh_1D.o
	$(f90comp) -c $(fflags) $<
boundary_conditions.o: boundary_conditions.f90 initial_condition.o global.mod user_definitions.o generate_mesh_1D.o
	$(f90comp) -c $(fflags) $<
initial_condition.o: initial_condition.f90 global.mod user_definitions.o generate_mesh_1D.o
	$(f90comp) -c $(fflags) $<
generate_mesh_1D.o: generate_mesh_1D.f90 global.mod user_definitions.o
	$(f90comp) -c $(fflags) $<
user_definitions.o: user_definitions.f90 global.mod
	$(f90comp) -c $(fflags) $<
global.mod global.o: global.f90
	$(f90comp) -c $(fflags) $<


# Cleaning everything
clean:
	rm -vf hmX_1D *.mod
	rm $(objects)
# End of the makefile
