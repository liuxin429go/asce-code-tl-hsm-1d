subroutine Central_upwind_method
use global
implicit none


do i=1,nnode
    !axp(i)=dmax1(UL(i,2)+dsqrt(g*h2L(i)), UR(i,2)+dsqrt(g*h2R(i)), 0.0d0);  ! a_in and a_out for central-upwind scheme
    !axm(i)=dmin1(UL(i,2)-dsqrt(g*h2L(i)), UR(i,2)-dsqrt(g*h2R(i)), 0.0d0);
	!ump(i)=(UR(i,2)*h2R(i)+UR(i,4)*h1R(i))/(dmax1(h2R(i)+h1R(i),1.0d-13));
	!umm(i)=(UL(i,2)*h2L(i)+UL(i,4)*h1L(i))/(dmax1(h2L(i)+h1L(i),1.0d-13));	
	!axp(i)=dmax1( ump(i)+dsqrt(g*(h2R(i)+h1R(i))), umm(i)+dsqrt(g*(h2L(i)+h1L(i))), 1.0d-13);
	!axm(i)=dmin1( ump(i)-dsqrt(g*(h2R(i)+h1R(i))), umm(i)-dsqrt(g*(h2L(i)+h1L(i))), -1.0d-13);
	axp(i)=dmax1( UL(i,2)+dsqrt(g*h1L(i)), UR(i,2)+dsqrt(g*h1R(i)),&
		UL(i,4)+dsqrt(g*h1L(i)+g*h2L(i)+g*hcL(i)*(rhos-rhow)/(2.0d0*UL(i,6)*(rhos-rhow)+2.0d0*rhow)),&
		UR(i,4)+dsqrt(g*h1R(i)+g*h2R(i)+g*hcR(i)*(rhos-rhow)/(2.0d0*UR(i,6)*(rhos-rhow)+2.0d0*rhow)),1.0d-10);
	axm(i)=dmin1( UL(i,2)-dsqrt(g*h1L(i)), UR(i,2)-dsqrt(g*h1R(i)),&
		UL(i,4)-dsqrt(g*h1L(i)+g*h2L(i)+g*hcL(i)*(rhos-rhow)/(2.0d0*UL(i,6)*(rhos-rhow)+2.0d0*rhow)),&
		UR(i,4)-dsqrt(g*h1R(i)+g*h2R(i)+g*hcR(i)*(rhos-rhow)/(2.0d0*UR(i,6)*(rhos-rhow)+2.0d0*rhow)),-1.0d-10);
	!axp(i)=1.05d0*axp(i)
	!axm(i)=1.05d0*axm(i)
	!axp(i)=dmax1( UL(i,2)+dsqrt(g*h1L(i)), UR(i,2)+dsqrt(g*h1R(i)),&
	!	UL(i,4)+dsqrt(g*h2L(i)+g*h1L(i)),UR(i,4)+dsqrt(g*h2R(i)+g*h1R(i)),0.0d0);
	!axm(i)=dmin1( UL(i,2)-dsqrt(g*h1L(i)), UR(i,2)-dsqrt(g*h1R(i)),&
	!	UL(i,4)-dsqrt(g*h2L(i)+g*h1L(i)),UR(i,4)-dsqrt(g*h2R(i)+g*h1R(i)),0.0d0);
	
    do k=1,mvariables
       if(dabs(axp(i))+dabs(axm(i))>1.0d-10)then
	       if(k==1) F(i,k)=(axp(i)*FL(i,k)-axm(i)*FR(i,k)+axp(i)*axm(i)*(UR(i,k)-UL(i,k)))/(axp(i)-axm(i));
		   if(k==2) F(i,k)=(axp(i)*FL(i,k)-axm(i)*FR(i,k)+axp(i)*axm(i)*(UR(i,k)*h1R(i)-UL(i,k)*h1L(i)))/(axp(i)-axm(i));
	       if(k==3) F(i,k)=(axp(i)*FL(i,k)-axm(i)*FR(i,k)+axp(i)*axm(i)*(UR(i,k)-UL(i,k)))/(axp(i)-axm(i));
		   if(k==4) F(i,k)=(axp(i)*FL(i,k)-axm(i)*FR(i,k)+axp(i)*axm(i)*(UR(i,k)*h2R(i)-UL(i,k)*h2L(i)))/(axp(i)-axm(i));
		   if(k==5) F(i,k)=(axp(i)*FL(i,k)-axm(i)*FR(i,k)+axp(i)*axm(i)*(UR(i,k)-UL(i,k)))/(axp(i)-axm(i));
		   !if(k==6) F(i,k)=(axp(i)*FL(i,k)-axm(i)*FR(i,k)+axp(i)*axm(i)*(UR(i,k)*h2R(i)-UL(i,k)*h2L(i)))/(axp(i)-axm(i));
		   if(k==6) F(i,k)=(axp(i)*FL(i,k)-axm(i)*FR(i,k)+axp(i)*axm(i)*(UR(i,k)*(UR(i,3)-0.5d0*(UL(i,7)+UR(i,7)))-UL(i,k)&
					*(UL(i,3)-0.5d0*(UL(i,7)+UR(i,7)))))/(axp(i)-axm(i));
		   if(k==7) F(i,k)=0.0d0;
		   !else
           !   F(i,k)=(axp(i)*FL(i,k)-axm(i)*FR(i,k)+axp(i)*axm(i)*(UR(i,k)-UL(i,k)))/(axp(i)-axm(i));    !central-upwind scheme
		   !endif  
       else
           F(i,k)=0.5d0*(FR(i,k)+FL(i,k))
       endif
    enddo
    dtlimit(i)=delx/dmax1(axp(i),axm(i),1.0d-10);     ! CFL limited time step for each cell
enddo

end subroutine Central_upwind_method
