subroutine boundary_conditions
use global
implicit none


! set the ghost sided-values of a bounadary
do k=1,mvariables
    UL(1,k)=UR(1,k)       ! free outlet bc
    !UL(1,k)=UL(nnode,k)       ! periodic bc
enddo

do k=1,mvariables
    UR(nnode,k)=UL(nnode,k)        ! free outlet bc
    !UR(nnode,k)=UR(1,k)        ! periodic bc
enddo


end subroutine boundary_conditions
