subroutine SW_Solver_1D    ! compute governing equations
use global
implicit none

h1old0(1:nele)=h1old(1:nele); uh1old0(1:nele)=uh1old(1:nele); 
uh2old0(1:nele)=uh2old(1:nele); eta2old0(1:nele)=eta2old(1:nele);
hcold0(1:nele)=hcold(1:nele); Reta2old0(1:nele)=Reta2old(1:nele);
Bold0(1:nele)=Bold(1:nele);

Ustar=0.0d0
do i=1,nele
	Ustar=dmin1( Ustar,dabs(u2old(i)) );  
enddo



do rkd=1,2                          

do i=1,nele   ! set unknown variable vector using their cell centered values
    Uc(i,1)=h1old(i); Uc(i,2)=u1old(i);  Uc(i,3)=eta2old(i); Uc(i,4)=u2old(i);
	Uc(i,5)=Reta2old(i); Uc(i,6)=cold(i); Uc(i,7)=Bold(i);
enddo

!!== choose a MUSCL limiter, compute the limited slopes
call generalized_minmod_limiter

!!== piecewise linear reconstruction for primary variables
!!== compute left and right-sided values for each interface
do i=1,nnode
   do k=1,mvariables
       if(i<nnode) UR(i,k)=Uc(i,k)-0.5d0*delx*lmx(i,k);
       if(i>1)  UL(i,k)=Uc(i-1,k)+0.5d0*delx*lmx(i-1,k);
   enddo
enddo

call Surface_positivity_correction
!call auxiliary_surface_level
!call edge_values_correction
call boundary_conditions

!!- compute the RHS/LHS physical convective fluxes
do i=1,nnode
    h1R(i)=UR(i,1) !dmax1(UR(i,3)-UR(i,1),0.0d0); 
	h1L(i)=UL(i,1) !dmax1(UL(i,3)-UL(i,1),0.0d0);
    h2R(i)=dmax1(UR(i,3)-UR(i,7),0.0d0); h2L(i)=dmax1(UL(i,3)-UL(i,7),0.0d0);  !just to avoid possible negative machine error
	h3R(i)=dmax1(UR(i,5)-UR(i,7),0.0d0); h3L(i)=dmax1(UL(i,5)-UL(i,7),0.0d0);
	hcR(i)=h2R(i)*UR(i,6); hcL(i)=h2L(i)*UL(i,6);
	rho2R(i)=UR(i,6)*rhos+(1.0d0-UR(i,6))*rhow;
	rho2L(i)=UL(i,6)*rhos+(1.0d0-UL(i,6))*rhow;
	
	FR(i,1)=UR(i,2)*h1R(i); 
	FL(i,1)=UL(i,2)*h1L(i);
	FR(i,2)=UR(i,2)*UR(i,2)*h1R(i)+0.5d0*g*h1R(i)*h1R(i);   
	FL(i,2)=UL(i,2)*UL(i,2)*h1L(i)+0.5d0*g*h1L(i)*h1L(i);
	FR(i,3)=UR(i,4)*h2R(i); 
	FL(i,3)=UL(i,4)*h2L(i); 
	FR(i,4)=UR(i,4)*UR(i,4)*h2R(i)+0.5d0*g*h2R(i)*h2R(i)+g*h1R(i)*h2R(i);
	FL(i,4)=UL(i,4)*UL(i,4)*h2L(i)+0.5d0*g*h2L(i)*h2L(i)+g*h1L(i)*h2L(i);
	FR(i,5)=Ustar*h3R(i);
	FL(i,5)=Ustar*h3L(i);
	FR(i,6)=h2R(i)*UR(i,4)*UR(i,6);
	FL(i,6)=h2L(i)*UL(i,4)*UL(i,6);
	FR(i,7)=0.0d0;
	FL(i,7)=0.0d0;
enddo

call Central_upwind_method

!!!====== global times step =======!!!
if(rkd==1)then  
   dt=1.0d+10                   ! initial guess of time step for the first step
   do i=1,nnode
      dt=dmin1(dt,dtlimit(i))   ! minimum allowed dt over the entire domain
   enddo
   dt=CFL*dt
   if((time+dt)>totaltime) dt=dmin1(totaltime-time,dt)
endif


dtedge1(1:nnode)=dt; dtedge2(1:nnode)=dt;  dtedge3(1:nnode)=dt;  ! initialization of dtedge for both layers

call Point_source_terms
!call local_draining_time_step
!! compute the fully discretized ODEs of the governing system

do i=1,nnode
	Cnc2(i)=-0.5d0*g*(UR(i,1)+UL(i,1))*(UR(i,5)-UL(i,5));
	Cnc4(i)=0.5d0*g*(h2R(i)+h2L(i))*( h1R(i)-h1L(i) ) &
			-0.5d0*g*rhow*( h2R(i)/rho2R(i)+ h2L(i)/rho2L(i) )*(h1R(i)-h1L(i)) &
			-0.5d0*g*(h2R(i)+h2L(i))*(UR(i,7)-UL(i,7)) &
			+0.5d0*g*(h1R(i)+h1L(i))*(h3R(i)-h3L(i)) &
			+0.25d0*g*(rhos-rhow)*( hcR(i)/rho2R(i)+hcL(i)/rho2L(i) )*(h3R(i)-h3L(i))&
			-0.25d0*g*(rhos-rhow)*( h2R(i)/rho2R(i)+h2L(i)/rho2L(i) )*(hcR(i)-hcL(i));
		
enddo	

!!compute the gradients for the source terms
do i=1,nele
	h1x(i)=(UL(i+1,1)-UR(i,1))/delx
	Bxs(i)=(UL(i+1,7)-UR(i,7))/delx
enddo


do i=1,nele   ! source term approximation

	ncsource2(i)= -g*h1old(i)*(UL(i+1,5)-UR(i,5))/delx
			
	ncsource4(i)=g*(rho2(i)-rhow)/rho2(i)*(eta2old(i)-Bold(i))*h1x(i)-g*(eta2old(i)-Bold(i))*Bxs(i) &		
		+g*h1old(i)*(h3L(i+1)-h3R(i))/delx &
		+g*(rhos-rhow)/(2.0d0*rho2(i))*hcold(i)*(h3L(i+1)-h3R(i))/delx&
		-g*(rhos-rhow)/(2.0d0*rho2(i))*(Reta2old(i)-Bold(i))*(hcL(i+1)-hcR(i))/delx
	
	CC2(i)=axp(i)/(axp(i)-axm(i))*Cnc2(i)/delx-axm(i+1)/(axp(i+1)-axm(i+1))*Cnc2(i+1)/delx
	CC4(i)=axp(i)/(axp(i)-axm(i))*Cnc4(i)/delx-axm(i+1)/(axp(i+1)-axm(i+1))*Cnc4(i+1)/delx

enddo

do i=1,nele   ! friction forces
	if(h1old(i)>1.0d-3)then
		fman1(i)=rhow*g*man1*man1*(u1old(i)-u2old(i))*dabs(u1old(i)-u2old(i))/h1old(i)**(1.0d0/3.0d0)
	else
		fman1(i)=0.0d0
	endif
	
	if(eta2old(i)>(Bold(i)+1.0d-3))then
		fman2(i)=rho2(i)*g*man2*man2*u2old(i)*dabs(u2old(i))
	else
		fman2(i)=0.0d0
	endif   
enddo	   

!!=== Explicit part of numerical method 
do i=1,nele    
	h1old(i)=h1old(i)-dt*(F(i+1,1)-F(i,1))/delx+dt*psR1(i);
	uh1old(i)=uh1old(i)-dt*(F(i+1,2)-F(i,2))/delx+dt*ncsource2(i)+dt*psR2(i)+dt*CC2(i)-dt*fman1(i)/rhow
	eta2old(i)=eta2old(i)-dt*(F(i+1,3)-F(i,3))/delx+dt*psR3(i);
	uh2old(i)=uh2old(i)-dt*(F(i+1,4)-F(i,4))/delx+dt*ncsource4(i)+dt*psR4(i)+dt*CC4(i)+dt*(fman1(i)-fman2(i))/rho2(i) 
	Reta2old(i)=Reta2old(i)-dt*(F(i+1,5)-F(i,5))/delx
	hcold(i)=hcold(i)-dt*(F(i+1,6)-F(i,6))/delx+dt*psR6(i)
	Bold(i)=Bold(i)+dt*psR7(i)
		
	if(eta2old(i)>(Bold(i)-1.0d-14))then   !only allows for negative water depth with machine errors 
	   eta2old(i)=dmax1(eta2old(i),Bold(i));  ! water depth is not forced to be positive
	else
	   write(*,*) "Warning: Negative fluid depth for lower layer in cell",i, eta2old(i)-Bold(i)
	   eta2old(i)=dmax1(eta2old(i),Bold(i));
	endif
	if(h1old(i)>-1.0d-14)then    
	   h1old(i)=dmax1(h1old(i),0.0d0);  
	else
		h1old(i)=dmax1(h1old(i),0.0d0);
	   write(*,*) "Warning: Negative fluid depth for upper layer in cell",i, h1old(i)
	endif
enddo

!!=== Partially Implicit treatment for fast relaxation term 
do i=1,nele	
	Reta2old(i)= eps/(eps-dt)*Reta2old(i)-dt/(eps-dt)*eta2old(i) 
enddo



if(rkd==1)then  
    do i=1,nele     
		h1old(i)=h1old(i)
		uh1old(i)=uh1old(i)
		eta2old(i)=eta2old(i)
		uh2old(i)=uh2old(i)
		Reta2old(i)=Reta2old(i)
		hcold(i)=hcold(i)
		Bold(i)=Bold(i)
		
		h2old(i)=dmax1(eta2old(i)-Bold(i),0.0d0);	
		eta1old(i)=h1old(i)+eta2old(i);
		u1old(i)=dsqrt(2.0d0)*h1old(i)*uh1old(i)/dsqrt( h1old(i)**4.0d0+dmax1(h1old(i)**4.0d0,1.0d-10) );
		u2old(i)=dsqrt(2.0d0)*h2old(i)*uh2old(i)/dsqrt( h2old(i)**4.0d0+dmax1(h2old(i)**4.0d0,1.0d-10) );
		cold(i)=dsqrt(2.0d0)*h2old(i)*hcold(i)/dsqrt( h2old(i)**4.0d0+dmax1(h2old(i)**4.0d0,1.0d-10) );    
	enddo	
elseif(rkd==2)then  
    do i=1,nele 
		h1old(i)=0.5d0*h1old0(i)+0.5d0*h1old(i)   
		uh1old(i)=0.5d0*uh1old0(i)+0.5d0*uh1old(i)  
		eta2old(i)=0.5d0*eta2old0(i)+0.5d0*eta2old(i)  
		uh2old(i)=0.5d0*uh2old0(i)+0.5d0*uh2old(i)  
		Reta2old(i)=0.5d0*Reta2old0(i)+0.5d0*Reta2old(i)  
		hcold(i)=0.5d0*hcold0(i)+0.5d0*hcold(i)  
		Bold(i)=0.5d0*Bold0(i)+0.5d0*Bold(i)  		
		
		h2old(i)=dmax1(eta2old(i)-Bold(i),0.0d0);	
		eta1old(i)=h1old(i)+eta2old(i);
		u1old(i)=dsqrt(2.0d0)*h1old(i)*uh1old(i)/dsqrt( h1old(i)**4.0d0+dmax1(h1old(i)**4.0d0,1.0d-10) );
		u2old(i)=dsqrt(2.0d0)*h2old(i)*uh2old(i)/dsqrt( h2old(i)**4.0d0+dmax1(h2old(i)**4.0d0,1.0d-10) );
		cold(i)=dsqrt(2.0d0)*h2old(i)*hcold(i)/dsqrt( h2old(i)**4.0d0+dmax1(h2old(i)**4.0d0,1.0d-10) ); 
	enddo

endif

enddo

! monitoring the interface instability
!do i=1,nele
!if( h1old(i)+h2old(i)>0.0d0 .and. (u1old(i)-u2old(i))**2.0d0>(1.0d0-rr)*g*(h1old(i)+h2old(i)) )then
!   write(*,*) "Warning: System is not hyperbolic in cell",i
!endif   
!enddo

end subroutine SW_Solver_1D
