use global
call user_definitions
call generate_mesh_1D
call init


totaltime=1.5d0    !total simulation time
time=0.0d0

open(1001,file='out.dat')        ! create the output file
!nzone=0
write(1001,*)'TITLE = "1D DATA"'
write(1001,*)'VARIABLES =x,h1,eta1,eta2,B,u1,u2'
close (1001)

!call output

kn=10              !how many zones
simutime=0.0d0     !(totaltime/kn)
do while (dabs(totaltime-time)>1.0d-10)
  ! simutime=simutime+(totaltime/kn)
   !do while (time<simutime)
         call SW_Solver_1D
         time=time+dt
         write(*,*)'time=',time,omegaV !rhow/rho2(20) !,h1R(i)-h1L(i)
   !enddo
   !call output
enddo



do i=1,nele
	open(3,file='./output/bottom.dat')
      write(3,*) xc(i), Bold(i)
    open(2,file='./output/eta2.dat')
      write(2,*) xc(i), eta2old(i) !-eta2t0(i)
	open(4,file='./output/eta1t0.dat')
      write(4,*) xc(i), h1t0(i)+eta2t0(i)
	open(5,file='./output/eta1.dat')
      write(5,*) xc(i), eta1old(i) !-(h1t0(i)+eta2t0(i))
	open(6,file='./output/eta2t0.dat')
      write(6,*) xc(i), eta2t0(i)
	open(7,file='./output/h1.dat')
      write(7,*) xc(i), h1old(i)
	open(8,file='./output/c.dat')
      write(8,*) xc(i), cold(i)
	open(9,file='./output/u1.dat')
      write(9,*) xc(i), u1old(i)
	open(10,file='./output/u2.dat')
      write(10,*) xc(i), u2old(i)
	open(11,file='./output/hc.dat')
      write(11,*) xc(i),hcold(i)
	open(12,file='./output/test1.dat')
      write(12,*) xc(i),Es(i)
	open(13,file='./output/froude1.dat')
      write(13,*) xc(i),dabs(u2old(i))/dsqrt(g*(eta2old(i)-Bold(i)))

enddo
!call compute_errors

end
