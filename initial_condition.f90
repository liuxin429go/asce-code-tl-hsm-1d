subroutine init     !define initial conditions
use global
implicit none

!=======define initial topography======!
do i=1,nele
	Bold(i)=0.0d0;
    !Bold(i)=-2.0d0;          ! define the bed levels on cell centers
	!if(xc(i)>=0.4d0 .and. xc(i)<=0.6d0)then
		!Bold(i)=0.2d0*(dcos(10.0d0*pi*(xc(i)-0.5d0))+1.0d0)-2.0d0
	!endif	
	!Bold(i)=0.01d0-0.005d0*(xc(i)+2.0d0)
enddo

!============== define initial condition ================!
do i=1,nele
	eta2old(i)=0.2d0 !-1.0d0 
	h1old(i)=3.0d0-eta2old(i) !1.0d0 
	cold(i)=0.02d0
	u2old(i)=0.0d0 
	u1old(i)=0.0d0
	if(xc(i)<0.0d0)then
		!h1old(i)=1.2d0-eta2old(i)
		cold(i)=0.59d0
		u2old(i)=0.0d0 
	endif
	
	!if(xc(i)<0.3d0 .and. xc(i)>0.2d0) h1old(i)=1.001d0
	!if(xc(i)<0.4d0) h1old(i)=0.3d0
	h2old(i)=eta2old(i)-Bold(i)

	
	Reta2old(i)=eta2old(i)
enddo

	uh2old(1:nele)=u2old(1:nele)*h2old(1:nele)
	uh1old(1:nele)=u1old(1:nele)*h1old(1:nele)
	hcold(1:nele)=cold(1:nele)*h2old(1:nele)

!initial values
	h1t0(1:nele)=h1old(1:nele);
	eta2t0(1:nele)=eta2old(1:nele); 
      

end subroutine init
