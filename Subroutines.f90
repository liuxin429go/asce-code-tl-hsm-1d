!!!===========================  minmod limiter  ==============================!!!
subroutine generalized_minmod_limiter
use global
implicit none
real(DP) wE(mvariables),wC(mvariables),wW(mvariables)
th=1.0d0                    !coefficient theta
    do i=1,nele
    do k=1,mvariables
        if(i==nele)then
            wE(k)=0.0d0;      ! free outflow bc
			!wE(k)=th*( Uc(1,k)-Uc(i,k) )/delx;      ! periodic bc
            wC(k)=( Uc(i,k)-Uc(i-1,k) )/(2.0d0*delx);
            wW(k)=th*( Uc(i,k)-Uc(i-1,k) )/delx;
        elseif(i==1)then
            wE(k)=th*( Uc(i+1,k)-Uc(i,k) )/delx;
            wC(k)=( Uc(i+1,k)-Uc(1,k) )/(2.0d0*delx);
            wW(k)=0.0d0 !th*( Uc(i,k)-Uc(nele,k) )/delx;      ! free outflow bc
        else
            wE(k)=th*( Uc(i+1,k)-Uc(i,k) )/delx;
            wC(k)=( Uc(i+1,k)-Uc(i-1,k) )/(2.0d0*delx);
            wW(k)=th*( Uc(i,k)-Uc(i-1,k) )/delx;
        endif
        !lmx(i,k)=Minmod(wE(k),wC(k),wW(k))
		lmx(i,k)=Minmod(wE(k),wW(k),wW(k))
    enddo
    enddo
end subroutine generalized_minmod_limiter

!!!!========================================================================!!!!
subroutine Surface_positivity_correction
use global
implicit none
!! Positivity Correction of the reconstructed surface levels
    do i=1,nele
        if(UR(i,3)<UR(i,7))then
            UR(i,3)=UR(i,7);
            UL(i+1,1)=UL(i+1,7)+2.0d0*(eta2old(i)-Bold(i));   
        endif  
        if(UL(i+1,3)<UL(i+1,7))then
            UL(i+1,3)=UL(i+1,7);
            UR(i,3)=UR(i,7)+2.0d0*(eta2old(i)-Bold(i));
        endif 
		if(UR(i,5)<UR(i,7))then
            UR(i,5)=UR(i,7);
            UL(i+1,5)=UL(i+1,7)+2.0d0*(Reta2old(i)-Bold(i));   
        endif  
        if(UL(i+1,5)<UL(i+1,7))then
            UL(i+1,5)=UL(i+1,7);
            UR(i,5)=UR(i,7)+2.0d0*(Reta2old(i)-Bold(i));
        endif 
		lmx(i,3)=(UL(i+1,3)-UR(i,3))/delx;
		lmx(i,5)=(UL(i+1,5)-UR(i,5))/delx;
    enddo
end subroutine Surface_positivity_correction


!!!==================== compute the errors ============================!!!
subroutine compute_errors
use global
implicit none
errh1=0.0d0; erreta2=0.0d0; erruh1=0.0d0; erruh2=0.0d0;
Linferrh1=0.0d0; Linferreta2=0.0d0; 
Linferruh1=0.0d0; Linferruh2=0.0d0;
do i=1,nele
   errh1=errh1+dabs(h1old(i)-h1t0(i));
   erreta2=erreta2+dabs(eta2old(i)-eta2t0(i));
   erruh1=erruh1+dabs(uh1old(i)-0.0d0);
   erruh2=erruh2+dabs(uh2old(i)-0.0d0);
   Linferrh1=dmax1(Linferrh1,dabs(h1old(i)-h1t0(i)));
   Linferreta2=dmax1(Linferreta2,dabs(eta2old(i)-eta2t0(i)));
   Linferruh1=dmax1(Linferruh1,dabs(uh1old(i)-0.0d0));
   Linferruh2=dmax1(Linferruh2,dabs(uh2old(i)-0.0d0));
enddo
errh1=errh1/nele; erreta2=erreta2/nele;
erruh1=erruh1/nele; erruh2=erruh2/nele;

open(9,file='./output/L1error.dat')
write(9,*) errh1,erreta2,erruh1,erruh2
open(10,file='./output/Linferror.dat')
write(10,*) Linferrh1,Linferreta2,Linferruh1,Linferruh2
end subroutine compute_errors

!!!========= Calculate Point_source_terms and E, D and Ew ========!!!
subroutine Point_source_terms
use global
implicit none

do i=1,nele
	RichNo(i)=g*(rhos/rhow-1.0d0)*hcold(i)/dmax1((u1old(i)-u2old(i))**2.0d0,1.0d-14);
	!Ew(i)=(u1old(i)-u2old(i))*0.001224d0/(0.01632d0+RichNo(i)**0.6d0)
	!Ew(i)=(u1old(i)-u2old(i))*0.00153d0/(0.0204d0+RichNo(i)**0.8d0)
	Ew(i)=dabs(u1old(i)-u2old(i))*0.075d0/(1.0d0+718.0d0*RichNo(i)**2.4)**0.5d0
	!Ew(i)=(u1old(i)-u2old(i))*0.0028d0/RichNo(i)**1.2d0
	rho2(i)=cold(i)*rhos+(1.0d0-cold(i))*rhow;
	tau1(i)=g*rhow*man1*man1*(u1old(i)-u2old(i))*dabs(u1old(i)-u2old(i))*h1old(i)**(5.0d0/3.0d0)&
			/dsqrt( h1old(i)**4.0d0+dmax1(h1old(i)**4.0d0,1.0d-8) )
	tau2(i)=g*rho2(i)*man2*man2*u2old(i)*dabs(u2old(i))*h2old(i)**(5.0d0/3.0d0)&
			/dsqrt( h2old(i)**4.0d0+dmax1(h2old(i)**4.0d0,1.0d-8) )
	cnb(i)=dmin1(2.0d0*cold(i),1.0d0-por)		
	!Ds(i)=omegaV*cold(i)*(1.0d0+31.5d0/(dsqrt(dmax1(dabs(tau2(i)),0.0001d0)/rho2(i))/omegaV)**1.46d0);	
	if(Rp0<=0.2d0) kappa0=4.65d0+19.5d0*dd/dmax1(h2old(i),1.0d-3)
	if(Rp0>0.2d0 .and. Rp0<=1.0d0) kappa0=(4.35d0+17.5d0*dd/dmax1(h2old(i),1.0d-3))/Rp0**0.03d0
	if(Rp0>1.0d0 .and. Rp0<=200.0d0) kappa0=(4.45d0+18.0d0*dd/dmax1(h2old(i),1.0d-3))/Rp0**0.1d0
	if(Rp0>200.0d0 .and. Rp0<=500.0d0) kappa0=4.45d0/Rp0**0.1d0
	if(Rp0>500.0d0) kappa0=2.39d0
	Ds(i)=omegaV*cnb(i)*(1.0d0-cnb(i))**kappa0  ! Deposition
	!tau20(i)=g*rho2(i)*man2*man2*u1old(i)*dabs(u1old(i))*h2old(i)**(5.0d0/3.0d0)&
	!		/dsqrt( h2old(i)**4.0d0+dmax1(h2old(i)**4.0d0,1.0d-8) )
	!phi9(i)=dsqrt( dabs(tau2(i)/rho2(i))*dsqrt(g*(rhos/rhow-1.0d0)*dd**3.0d0)/nnu )/omegaV
!! JHR Es
	!Reynold(i)=dd*dsqrt(g*(rhos/rhow-1.0d0)*dd)/nnu     ! particle Reynolds number          
	!if(Reynold(i)>=282.84d0)then
	!	thetac(i)=0.045d0
	!elseif(Reynold(i)<=6.61d0)then
	!	thetac(i)=0.1414d0/Reynold(i)**0.2306d0
	!else
	!	thetac(i)=(1.0d0+(0.0223d0*Reynold(i))**2.8358d0)**0.3542d0/(3.0946d0*Reynold(i)**0.6769d0)
	!endif
	!stress(i)=man2*man2*u2old(i)*dabs(u2old(i))*h2old(i)**(5.0d0/3.0d0)&
	!		/dsqrt( h2old(i)**4.0d0+dmax1(h2old(i)**4.0d0,1.0d-9) )/(rhos/rhow-1.0d0)/dd
	!qb0(i)=6.6d0*8.0d0*dsqrt((rhos/rhow-1.0d0)*g*dd**3.0d0)*dmax1(dabs(stress(i))-thetac(i),0.0d0)**1.5d0
	!if(uh2old(i)>=0.0d0)then
	!ce(i)=dabs(qb0(i))/dmax1(dabs(uh2old(i)),0.001d0)	
	!else
	!ce(i)=qb0(i)/dmin1(uh2old(i),-0.001d0)
	!endif
	!Es(i)=omegaV*ce(i)*(1.0d0-ce(i))**(4.45d0/Rp0)

!! JHE E
	!if(phi9(i)>=13.2d0)then
	!	Es(i)=0.3d0
	!elseif(phi9(i)<=5.0d0)then
	!	Es(i)=0.0d0
	!else
	!	Es(i)=(3.0d-12)*phi9(i)**10.0d0*(1.0d0-5.0d0/phi9(i))
	!endif

	
!! Lai et al. 2015 modified Wright and Parker(2004)	E
	Sfx(i)=man2*man2*uh2old(i)*uh2old(i)/dmax1(h2old(i),0.001d0)**(10.0d0/3.0d0)
	usk(i)=dsqrt( g*(dd*rhos/rhow-dd)*(0.06d0+0.4d0*(h2old(i)*Sfx(i)/(dd*rhos/rhow-dd))**2.0d0) )	
	if(Rp1>2.36d0) XE(i)=1.0d0*usk(i)*(Rp1**0.6d0)*Sfx(i)**0.08d0/(omegaV*(1.0d0-cnb(i))**kappa0)
	if(Rp1<=2.36d0) XE(i)=0.586d0*usk(i)*(Rp1**1.23d0)*Sfx(i)**0.08d0/(omegaV*(1.0d0-cnb(i))**kappa0)
	Es(i)=1.0d0*7.8d-7*XE(i)**5.0d0/(1.0d0+7.8d-7/0.3d0*XE(i)**5.0d0)*omegaV*(1.0d0-cnb(i))**kappa0
	!XE(i)=
	!Ds(i)=0.0d0; Es(i)=0.0d0;
enddo

do i=1,nele
	psR1(i)=-Ew(i);
	psR2(i)=-Ew(i)*u1old(i)  !-tau1(i)/rhow;
	psR3(i)=Ew(i);
	psR4(i)=-(rho0-rho2(i))*(Es(i)-Ds(i))*u2old(i)/(1.0d0-por)/rho2(i)&
			+(1.0d0-rhow/rho2(i))*Ew(i)*u2old(i)+rhow/rho2(i)*Ew(i)*u1old(i)   !(tau1(i)-tau2(i))/rho2(i)
	psR5(i)=0.0d0;
	psR6(i)=Es(i)-Ds(i);
	psR7(i)=-(Es(i)-Ds(i))/(1.0d0-por)
enddo
do i=1,nele
	!psR1(i)=0.0d0;psR2(i)=0.0d0;psR3(i)=0.0d0;psR4(i)=0.0d0;psR5(i)=0.0d0;psR6(i)=0.0d0;psR7(i)=0.0d0;
enddo

end subroutine Point_source_terms

!!!========= Generate the tecplot style output files ========!!!
subroutine output
use global
implicit none
	
!real*8   h1node(200000),eta1node(200000),eta2node(200000),Bnode(200000),u1node(200000),u2node(200000)
!integer  ipoin

open(10,file='out.dat',access='append')
!nzone=0

!x,h1,eta1,eta2,B,u1,u2
! tecplot data format output
	!write(1001,1)istep,nnode 
	do i=1,nnode
		if(i==1)then 
			write(10,*)x(i),h1old(i),eta1old(i),eta2old(i),Bold(i),u1old(i),u2old(i) !,vnode(i) !,znode(i)         !,metaanode(i),tanode(i)  !,meta(i)
		elseif(i==nnode)then
			write(10,*)x(i),h1old(i-1),eta1old(i-1),eta2old(i-1),Bold(i-1),u1old(i-1),u2old(i-1)
		else
			write(10,*)x(i),0.5d0*(h1old(i-1)+h1old(i)),0.5d0*(eta1old(i-1)+eta1old(i)),0.5d0*(eta2old(i-1)+eta2old(i))&
				,0.5d0*(Bold(i-1)+Bold(i)),0.5d0*(u1old(i-1)+u1old(i)),0.5d0*(u2old(i-1)+u2old(i))
		endif
	end do
	write(1001,*)0.0d0,0.0d0
	close(1001)

1    format('ZONE T="',I8,'" I=',I8,' F=POINT')
	
end subroutine output