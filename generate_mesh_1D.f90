subroutine generate_mesh_1D
use global
implicit none


!---NODE GENERATION
	delx=lx/nele   ! dimension of each cell
    
	k=0 !define an indicator
	do i=1,nnode
	   k=k+1
	   nodnumber(i)=k !assign the number to each node
       x(nodnumber(i))=minx+(i-1)*delx   !coordinates of each node     
    enddo


    do i=1,nele
       xc(i)=minx+(i-1)*delx+0.5d0*delx  !0.5d0*( x(i)+x(i+1) );  !coordinates of each cell center 
    enddo

end subroutine generate_mesh_1D